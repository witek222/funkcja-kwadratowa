package com.demo.springboot.rest;

import com.demo.springboot.dto.ResultDto;
import com.demo.springboot.service.QuadraticFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class FunctionApiController {

    private static final Logger LOGGER = LoggerFactory.getLogger(FunctionApiController.class);

    private QuadraticFunction quadraticFunction;

    @Autowired
    public FunctionApiController(QuadraticFunction quadraticFunction) {
        this.quadraticFunction = quadraticFunction;
    }



    @GetMapping ("/api/math/quadratic-function")
    public ResponseEntity<ResultDto> calculateFunction(@RequestParam Double a, @RequestParam Double b,@RequestParam Double c){

        LOGGER.info("--- a: {}", a);
        LOGGER.info("--- b: {}", b);
        LOGGER.info("--- c: {}", c);

        final ResultDto resultDto = quadraticFunction.calculateFunction(a, b, c);

        return ResponseEntity.ok(resultDto);
    }
}
